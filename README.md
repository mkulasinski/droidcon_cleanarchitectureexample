# README #
This repo represents a simple Business logic separated from Presentation. Project is about a Engine, Headlights and Battery. Where each of the features has some business logic that might affect other features.

## Battery ##
Battery has some capacity, when not charged but used it will go flat. Rules for battery are as follow:

* When battery is flat turn off Headlights

## Headlights ##
Headlights have some modes:

* Low Beam medium power use
* High Beam high power use
* Fog light low power use 

Rules are:

* When lights are on they will discharge battery with the rate given for each of the light mode
* When lights are off then battery is not being discharged

## Engine ##
Engine is probably most complex feature of this app. It has only two states on and off.
Rules for engine:

* upon ignition on headlights should turn on to Low Beam
* when engine is running charge battery with engine charging rate
* upon ignition off turn off headlight and stop charging