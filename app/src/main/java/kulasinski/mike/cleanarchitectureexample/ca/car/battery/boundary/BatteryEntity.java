package kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary;

public class BatteryEntity {
    public final int batteryCapacity;
    private final int currentLevel;
    public final int currentChargeRate;
    public final int currentUsageRate;
    private final long currentLevelLastTimestamp;

    public BatteryEntity(int batteryCapacity,
                         int currentLevel,
                         int currentChargeRate,
                         int currentUsageRate) {
        this.batteryCapacity = batteryCapacity;
        this.currentLevel = currentLevel;
        this.currentChargeRate = currentChargeRate;
        this.currentUsageRate = currentUsageRate;
        this.currentLevelLastTimestamp = System.currentTimeMillis();
    }

    public int calculateCurrentChargeLevel() {
        long currentTimeMillis = System.currentTimeMillis();
        long timeElapsed = currentTimeMillis - currentLevelLastTimestamp;

        // delta per second
        int chargeDischargeDelta = currentChargeRate - currentUsageRate;

        float secondsElapsed = timeElapsed / 1000;
        int newLevel = (int) (currentLevel + chargeDischargeDelta * secondsElapsed);
        newLevel = Math.min(batteryCapacity, newLevel);
        return Math.max(0, newLevel);
    }
}
