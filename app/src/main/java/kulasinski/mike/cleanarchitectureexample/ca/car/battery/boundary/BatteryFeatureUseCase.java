package kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.repository.BatteryRepository;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightFeatureBoundary;

class BatteryFeatureUseCase implements BatteryFeatureBoundary {

    @NonNull
    private final BatteryRepository mBatteryRepository;
    @NonNull
    private final HeadlightFeatureBoundary mHeadlightFeatureBoundary;

    BatteryFeatureUseCase() {
        mBatteryRepository = BatteryRepository.Factory.getRepository();
        mHeadlightFeatureBoundary = HeadlightFeatureBoundary.Factory.create();
    }

    BatteryFeatureUseCase(@NonNull HeadlightFeatureBoundary headlightFeatureBoundary) {
        mBatteryRepository = BatteryRepository.Factory.getRepository();
        mHeadlightFeatureBoundary = headlightFeatureBoundary;
    }

    @Override
    public void requestBattery(@NonNull Callback callback) {
        BatteryEntity battery = mBatteryRepository.getBattery();
        if (battery == null) {
            return;
        }
        updateDelta(0, 0);
        callback.onBatteryEntityAvailable(battery);
    }

    @Override
    public void addChargingRate(int rateDelta) {
        updateDelta(0, rateDelta);
    }

    @Override
    public void addUsageRate(int rateDelta) {
        updateDelta(rateDelta, 0);
    }

    private void updateDelta(int usageRateDelta, int chargeRateDelta) {
        BatteryEntity battery = mBatteryRepository.getBattery();
        if (battery == null) {
            return;
        }
        mBatteryRepository.updateBattery(battery, chargeRateDelta, usageRateDelta);
        int chargeLevel = battery.calculateCurrentChargeLevel();
        if (chargeLevel <= 0) {
            mHeadlightFeatureBoundary.turnLightsOff();
        }
    }
}
