package kulasinski.mike.cleanarchitectureexample.ca.car.battery.presenter;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryFeatureBoundary;

class BatteryFeaturePresenterImpl implements BatteryFeaturePresenter {

    @NonNull
    private final View mView;
    @NonNull
    private final BatteryFeatureBoundary mBatteryFeatureBoundary;

    BatteryFeaturePresenterImpl(@NonNull View view) {
        mView = view;
        mBatteryFeatureBoundary = BatteryFeatureBoundary.Factory.create();
    }

    @Override
    public void onRequestBatteryInfo() {
        mBatteryFeatureBoundary.requestBattery(
                new BatteryFeatureBoundary.Callback() {
                    @Override
                    public void onBatteryEntityAvailable(@NonNull BatteryEntity batteryEntity) {
                        updateUi(batteryEntity);
                    }
                });
    }

    private void updateUi(@NonNull BatteryEntity batteryEntity) {
        BatteryPresenterModel presenterModel = BatteryPresenterModel.transform(batteryEntity);
        mView.onBatteryInfo(presenterModel);
        if (presenterModel.percentage > 70) {
            mView.onChangeColorToHigh();
        } else if (presenterModel.percentage > 30) {
            mView.onChangeColorToNormal();
        } else {
            mView.onChangeColorToLow();
        }
        if (presenterModel.percentage == 0) {
            mView.onBatteryEmpty();
        }
    }
}
