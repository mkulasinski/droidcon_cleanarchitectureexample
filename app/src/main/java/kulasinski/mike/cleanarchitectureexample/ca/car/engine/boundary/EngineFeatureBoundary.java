package kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.repository.EngineRepository;

public interface EngineFeatureBoundary {

    @Nullable
    EngineEntity getEngine();

    void ignitionOn();

    void ignitionOff();

    class Factory {
        @NonNull
        public static EngineFeatureBoundary create() {
            return new EngineFeatureUseCase(EngineRepository.Factory.create());
        }
    }
}