package kulasinski.mike.cleanarchitectureexample.ca.car.battery.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;

public interface BatteryRepository {

    void addBattery(@NonNull BatteryEntity batteryEntity);

    @Nullable
    BatteryEntity getBattery();

    void updateBattery(@NonNull BatteryEntity entity, int chargeRateDelta, int dischargeRateDelta);

    class Factory {
        private static final BatteryRepository sRepository = new BatteryRepositoryImpl();

        @NonNull
        public static BatteryRepository getRepository() {
            return sRepository;
        }
    }
}