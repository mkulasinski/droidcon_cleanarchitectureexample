package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary;

import android.support.annotation.NonNull;

public class HeadlightEntity {

    @NonNull
    public final State state;
    @NonNull
    public final LightMode mode;
    public final int[] dischargeRates;

    public HeadlightEntity(@NonNull State state, @NonNull LightMode mode, int... dischargeRates) {
        this.state = state;
        this.mode = mode;
        this.dischargeRates = dischargeRates;
    }

    public int getDischargeRate(@NonNull LightMode mode) {
        return dischargeRates[mode.ordinal()];
    }

    public enum State {
        LIGHTS_ON,
        LIGHTS_OFF
    }

    public enum LightMode {
        LOW_BEAM,
        HIGH_BEAM,
        FOG_LIGHT
    }
}
