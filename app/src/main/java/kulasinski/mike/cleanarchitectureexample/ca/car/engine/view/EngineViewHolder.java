package kulasinski.mike.cleanarchitectureexample.ca.car.engine.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Switch;

import kulasinski.mike.cleanarchitectureexample.R;

class EngineViewHolder {

    @NonNull
    final Switch mEngineSwitch;

    public EngineViewHolder(@NonNull View root) {
        View container = root.findViewById(R.id.engineFeature);
        mEngineSwitch = (Switch) container.findViewById(R.id.engineFeature_switch);
    }
}
