package kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightFeatureBoundary;

public interface BatteryFeatureBoundary {

    void requestBattery(@NonNull Callback callback);

    void addChargingRate(int rateDelta);

    void addUsageRate(int rateDelta);

    interface Callback {
        void onBatteryEntityAvailable(@NonNull BatteryEntity batteryEntity);
    }

    class Factory {

        @NonNull
        public static BatteryFeatureBoundary create() {
            return new BatteryFeatureUseCase();
        }

        @NonNull
        public static BatteryFeatureBoundary create(@NonNull HeadlightFeatureBoundary headlightBoundary) {
            return new BatteryFeatureUseCase(headlightBoundary);
        }
    }
}