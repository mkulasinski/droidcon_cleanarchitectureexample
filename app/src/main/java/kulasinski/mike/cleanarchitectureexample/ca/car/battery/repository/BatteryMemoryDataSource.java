package kulasinski.mike.cleanarchitectureexample.ca.car.battery.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;

class BatteryMemoryDataSource implements BatteryRepository {
    private BatteryEntity mBatteryEntity;

    @Override
    public void addBattery(@NonNull BatteryEntity batteryEntity) {
        mBatteryEntity = batteryEntity;
    }

    @Nullable
    @Override
    public BatteryEntity getBattery() {
        return mBatteryEntity;
    }

    @Override
    public void updateBattery(@NonNull BatteryEntity entity, int chargeRateDelta, int dischargeRateDelta) {
        mBatteryEntity = new BatteryEntity(
                entity.batteryCapacity,
                entity.calculateCurrentChargeLevel(),
                entity.currentChargeRate + chargeRateDelta,
                entity.currentUsageRate + dischargeRateDelta
        );
    }
}
