package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.ui.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import kulasinski.mike.cleanarchitectureexample.R;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.presenter.HeadlightPresenter;

class HeadlightViewBinder {

    @NonNull
    private final HeadlightViewHolder mViewHolder;

    @NonNull
    private final LightModeChanged mLightModeCallback;

    public HeadlightViewBinder(@NonNull View view,
                               @NonNull LightModeChanged lightModeCallback) {
        mViewHolder = new HeadlightViewHolder(view);
        mLightModeCallback = lightModeCallback;

        mViewHolder.mLightMode.setOnCheckedChangeListener(mOnCheckedChangeListener);
        mViewHolder.mHeadlightSwitch.setOnCheckedChangeListener(mOnCheckedChangeListenerButton);
    }

    public void bind(@NonNull HeadlightPresenter.HeadlightPresenterModel model) {
        boolean enabled = model.state == HeadlightEntity.State.LIGHTS_ON;
        mViewHolder.mHeadlightSwitch.setOnCheckedChangeListener(null);
        mViewHolder.mHeadlightSwitch.setChecked(enabled);
        mViewHolder.mHeadlightSwitch.setOnCheckedChangeListener(mOnCheckedChangeListenerButton);
        for (int i = 0; i < mViewHolder.mLightMode.getChildCount(); i++) {
            mViewHolder.mLightMode.getChildAt(i).setEnabled(enabled);
        }
        mViewHolder.mLightMode.setOnCheckedChangeListener(null);
        switch (model.lightMode) {
            case LOW_BEAM:
                mViewHolder.mLightMode.check(R.id.headlightFeature_mode_lowBeam);
                break;
            case HIGH_BEAM:
                mViewHolder.mLightMode.check(R.id.headlightFeature_mode_highBeam);
                break;
            case FOG_LIGHT:
                mViewHolder.mLightMode.check(R.id.headlightFeature_mode_fogLight);
                break;
        }
        mViewHolder.mLightMode.setOnCheckedChangeListener(mOnCheckedChangeListener);
    }

    private HeadlightEntity.LightMode getMode() {
        int checkedId = mViewHolder.mLightMode.getCheckedRadioButtonId();
        if (checkedId == R.id.headlightFeature_mode_lowBeam) {
            return HeadlightEntity.LightMode.LOW_BEAM;
        } else if (checkedId == R.id.headlightFeature_mode_highBeam) {
            return HeadlightEntity.LightMode.HIGH_BEAM;
        } else if (checkedId == R.id.headlightFeature_mode_fogLight) {
            return HeadlightEntity.LightMode.FOG_LIGHT;
        }
        return HeadlightEntity.LightMode.LOW_BEAM;
    }

    private void notifyCallback() {
        mLightModeCallback.onLightModeChanged(mViewHolder.mHeadlightSwitch.isChecked(), getMode());
    }

    private RadioGroup.OnCheckedChangeListener mOnCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            notifyCallback();
        }
    };

    @SuppressWarnings("FieldCanBeLocal")
    private final CompoundButton.OnCheckedChangeListener mOnCheckedChangeListenerButton = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            notifyCallback();
        }
    };

    public interface LightModeChanged {
        void onLightModeChanged(boolean isHeadlightOn, @NonNull HeadlightEntity.LightMode mode);
    }
}
