package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;

public interface HeadlightRepository {

    void addHeadlight(@NonNull HeadlightEntity entity);

    @Nullable
    HeadlightEntity getHeadlight();

    void setHeadlightState(@NonNull HeadlightEntity.State state);

    void setHeadlightMode(@NonNull HeadlightEntity.LightMode mode);

    class Factory {
        private static final HeadlightRepository sRepository = new HeadlightRepositoryImpl();

        @NonNull
        public static HeadlightRepository getRepository() {
            return sRepository;
        }
    }
}
