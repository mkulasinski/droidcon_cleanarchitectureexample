package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;

class HeadlightMemoryDataSource implements HeadlightRepository {
    private HeadlightEntity mHeadlightEntity;

    @Override
    public void addHeadlight(@NonNull HeadlightEntity entity) {
        mHeadlightEntity = entity;
    }

    @Nullable
    @Override
    public HeadlightEntity getHeadlight() {
        return mHeadlightEntity;
    }

    @Override
    public void setHeadlightState(@NonNull HeadlightEntity.State state) {
        mHeadlightEntity = new HeadlightEntity(
                state,
                mHeadlightEntity.mode,
                mHeadlightEntity.dischargeRates
        );
    }

    @Override
    public void setHeadlightMode(@NonNull HeadlightEntity.LightMode mode) {
        mHeadlightEntity = new HeadlightEntity(
                mHeadlightEntity.state,
                mode,
                mHeadlightEntity.dischargeRates
        );
    }
}
