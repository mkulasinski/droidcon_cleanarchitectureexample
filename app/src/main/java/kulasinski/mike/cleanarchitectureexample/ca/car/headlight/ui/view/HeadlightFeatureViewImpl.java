package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.ui.view;

import android.support.annotation.NonNull;
import android.view.View;

import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.presenter.HeadlightPresenter;

public class HeadlightFeatureViewImpl {

    @NonNull
    private final HeadlightViewBinder mHeadlightViewBinder;
    @NonNull
    private final HeadlightPresenter mPresenter;
    @NonNull
    private final Callback mCallback;

    public HeadlightFeatureViewImpl(@NonNull View root, @NonNull Callback callback) {
        mCallback = callback;
        mHeadlightViewBinder = new HeadlightViewBinder(root, new HeadlightViewBinder.LightModeChanged() {
            @Override
            public void onLightModeChanged(boolean isHeadlightOn, @NonNull HeadlightEntity.LightMode mode) {
                if (!isHeadlightOn) {
                    mPresenter.onTurnLightsOff();
                } else {
                    mPresenter.onTurnLightsOn(mode);
                }
                mCallback.onHeadlightStateChanged();
            }
        });
        mPresenter = HeadlightPresenter.Factory.create(getView());
    }

    public void requestInfo() {
        mPresenter.onRequestHeadlightInfo();
    }

    @NonNull
    private HeadlightPresenter.View getView() {
        return new HeadlightPresenter.View() {

            @Override
            public void headlightInfoAvailable(@NonNull HeadlightPresenter.HeadlightPresenterModel model) {
                mHeadlightViewBinder.bind(model);
            }

            @Override
            public void noPowerForHeadlights() {
                mCallback.noPowerForHeadlights();
            }
        };
    }

    public interface Callback {
        void noPowerForHeadlights();

        void onHeadlightStateChanged();
    }
}
