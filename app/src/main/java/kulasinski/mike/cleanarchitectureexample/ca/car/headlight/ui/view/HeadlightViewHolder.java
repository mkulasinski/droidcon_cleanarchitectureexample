package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.ui.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Switch;

import kulasinski.mike.cleanarchitectureexample.R;

class HeadlightViewHolder {

    @NonNull
    final Switch mHeadlightSwitch;
    @NonNull
    final RadioGroup mLightMode;

    public HeadlightViewHolder(@NonNull View root) {
        View container = root.findViewById(R.id.headlightFeature);
        mHeadlightSwitch = (Switch) container.findViewById(R.id.headlightFeature_state);
        mLightMode = (RadioGroup) container.findViewById(R.id.headlightFeature_mode);
    }
}
