package kulasinski.mike.cleanarchitectureexample.ca.car.engine.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;

/**
 * composition of datasources, only memory is allowed for now. but in future you can add more and manage threading
 * here.
 */
class EngineRepositoryImpl implements EngineRepository {

    private final EngineMemoryDataSource mEngineMemoryDataSource;

    EngineRepositoryImpl() {
        mEngineMemoryDataSource = new EngineMemoryDataSource();
        addEngine(new EngineEntity(
                EngineEntity.State.STOPPED,
                200));
    }

    @Override
    public void addEngine(@NonNull EngineEntity engine) {
        mEngineMemoryDataSource.addEngine(engine);
    }

    @Nullable
    @Override
    public EngineEntity getEngine() {
        return mEngineMemoryDataSource.getEngine();
    }

    @Override
    public void setEngineState(@NonNull EngineEntity.State state) {
        mEngineMemoryDataSource.setEngineState(state);
    }
}
