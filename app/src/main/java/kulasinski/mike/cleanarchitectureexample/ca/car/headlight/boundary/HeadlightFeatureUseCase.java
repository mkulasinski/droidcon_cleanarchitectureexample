package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryFeatureBoundary;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.repository.HeadlightRepository;

class HeadlightFeatureUseCase implements HeadlightFeatureBoundary {

    @NonNull
    private final HeadlightRepository mHeadlightRepository;
    @NonNull
    private final BatteryFeatureBoundary mBatteryFeatureBoundary;

    HeadlightFeatureUseCase() {
        mHeadlightRepository = HeadlightRepository.Factory.getRepository();
        mBatteryFeatureBoundary = BatteryFeatureBoundary.Factory.create(this);
    }

    @Nullable
    @Override
    public HeadlightEntity getHeadlights() {
        return mHeadlightRepository.getHeadlight();
    }

    @Override
    public void turnLightsOn(@NonNull final HeadlightEntity.LightMode mode, @NonNull final NoPowerErrorCallback errorCallback) {
        final HeadlightEntity headlight = mHeadlightRepository.getHeadlight();
        if (headlight == null ||
                (headlight.state == HeadlightEntity.State.LIGHTS_ON && headlight.mode == mode)) {
            return;
        }
        final int dischargeRate;
        if (headlight.state == HeadlightEntity.State.LIGHTS_ON) {
            dischargeRate = headlight.getDischargeRate(mode) - headlight.getDischargeRate(headlight.mode);
        } else {
            dischargeRate = headlight.getDischargeRate(mode);
        }
        mBatteryFeatureBoundary.requestBattery(new BatteryFeatureBoundary.Callback() {
            @Override
            public void onBatteryEntityAvailable(@NonNull BatteryEntity batteryEntity) {
                if (batteryEntity.calculateCurrentChargeLevel() <= 0) {
                    errorCallback.insufficientPowerException();
                    return;
                }
                mBatteryFeatureBoundary.addUsageRate(dischargeRate);
                mHeadlightRepository.setHeadlightState(HeadlightEntity.State.LIGHTS_ON);
                mHeadlightRepository.setHeadlightMode(mode);
            }
        });
    }

    @Override
    public void turnLightsOff() {
        HeadlightEntity headlight = mHeadlightRepository.getHeadlight();
        if (headlight == null || headlight.state == HeadlightEntity.State.LIGHTS_OFF) {
            return;
        }
        mHeadlightRepository.setHeadlightState(HeadlightEntity.State.LIGHTS_OFF);
        mBatteryFeatureBoundary.addUsageRate(-headlight.getDischargeRate(headlight.mode));
    }

    @Override
    public void turnLightsDefaultState() {
        turnLightsOn(HeadlightEntity.LightMode.LOW_BEAM, new NoPowerErrorCallback() {
            @Override
            public void insufficientPowerException() {
                turnLightsOff();
            }
        });
    }
}
