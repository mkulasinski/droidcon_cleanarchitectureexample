package kulasinski.mike.cleanarchitectureexample.ca.car.engine.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;

class EngineMemoryDataSource implements EngineRepository {
    private EngineEntity mEngineEntity;

    @Override
    public void addEngine(@NonNull EngineEntity engine) {
        mEngineEntity = engine;
    }

    @Nullable
    @Override
    public EngineEntity getEngine() {
        return mEngineEntity;
    }

    @Override
    public void setEngineState(@NonNull EngineEntity.State state) {
        mEngineEntity = new EngineEntity(
                state,
                mEngineEntity.chargeRate
        );
    }
}
