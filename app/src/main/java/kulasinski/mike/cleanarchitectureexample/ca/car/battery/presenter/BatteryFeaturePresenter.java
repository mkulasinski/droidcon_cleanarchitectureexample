package kulasinski.mike.cleanarchitectureexample.ca.car.battery.presenter;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;

public interface BatteryFeaturePresenter {

    void onRequestBatteryInfo();

    interface View {
        void onBatteryInfo(@NonNull BatteryPresenterModel model);

        void onChangeColorToHigh();

        void onChangeColorToNormal();

        void onChangeColorToLow();

        void onBatteryEmpty();
    }

    class BatteryPresenterModel {
        public final float percentage;
        public final int chargeDelta;

        private BatteryPresenterModel(float percentage, int chargeDelta) {
            this.percentage = percentage;
            this.chargeDelta = chargeDelta;
        }

        public static BatteryPresenterModel transform(@NonNull BatteryEntity entity) {
            return new BatteryPresenterModel(
                    ((float) entity.calculateCurrentChargeLevel()) / ((float) entity.batteryCapacity) * 100,
                    entity.currentChargeRate - entity.currentUsageRate
            );
        }
    }

    class Factory {
        @NonNull
        public static BatteryFeaturePresenter create(@NonNull View view) {
            return new BatteryFeaturePresenterImpl(view);
        }
    }
}
