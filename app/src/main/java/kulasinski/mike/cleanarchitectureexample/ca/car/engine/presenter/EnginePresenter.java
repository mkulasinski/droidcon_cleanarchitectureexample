package kulasinski.mike.cleanarchitectureexample.ca.car.engine.presenter;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;

public interface EnginePresenter {

    void onRequestEngineInfo();

    void onTurnEngineOn();

    void onTurnEngineOff();

    interface View {
        void engineInfoAvailable(@NonNull EngineEntity engineEntity);
    }

    class Factory {
        @NonNull
        public static EnginePresenter create(@NonNull View view) {
            return new EnginePresenterImpl(view);
        }
    }
}
