package kulasinski.mike.cleanarchitectureexample.ca.car.battery.ui.view;

import android.support.annotation.NonNull;
import android.view.View;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.presenter.BatteryFeaturePresenter;

/**
 * Created by mike on 28/11/16.
 */

public class BatteryFeatureViewImpl {

    @NonNull
    private final BatteryViewBinder mBatteryViewBinder;
    @NonNull
    private final BatteryFeaturePresenter mPresenter;
    @NonNull
    private final Callback mCallback;

    public BatteryFeatureViewImpl(@NonNull View root, @NonNull Callback callback) {
        mBatteryViewBinder = new BatteryViewBinder(root);
        mCallback = callback;
        mPresenter = BatteryFeaturePresenter.Factory.create(getView());
    }

    public void requestInfo() {
        mPresenter.onRequestBatteryInfo();
    }

    @NonNull
    private BatteryFeaturePresenter.View getView() {
        return new BatteryFeaturePresenter.View() {
            @Override
            public void onBatteryInfo(@NonNull BatteryFeaturePresenter.BatteryPresenterModel model) {
                mBatteryViewBinder.bind(model);
            }

            @Override
            public void onChangeColorToHigh() {
                mBatteryViewBinder.changeColorToHight();
            }

            @Override
            public void onChangeColorToNormal() {
                mBatteryViewBinder.changeColorToNormal();
            }

            @Override
            public void onChangeColorToLow() {
                mBatteryViewBinder.changeColorToLow();
            }

            @Override
            public void onBatteryEmpty() {
                mCallback.onBatteryEmpty();
            }
        };
    }

    public interface Callback {
        void onBatteryEmpty();
    }
}
