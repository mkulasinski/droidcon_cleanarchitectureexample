package kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryFeatureBoundary;
import kulasinski.mike.cleanarchitectureexample.ca.car.engine.repository.EngineRepository;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightFeatureBoundary;

/**
 * Created by mike on 28/11/16.
 */
class EngineFeatureUseCase implements EngineFeatureBoundary {
    @NonNull
    private final EngineRepository mEngineRepository;
    @NonNull
    private final BatteryFeatureBoundary mBatteryFeatureBoundary;
    @NonNull
    private final HeadlightFeatureBoundary mHeadlightFeatureBoundary;

    EngineFeatureUseCase(@NonNull EngineRepository engineRepository) {
        mBatteryFeatureBoundary = BatteryFeatureBoundary.Factory.create();
        mHeadlightFeatureBoundary = HeadlightFeatureBoundary.Factory.create();
        mEngineRepository = engineRepository;
    }

    @Nullable
    @Override
    public EngineEntity getEngine() {
        return mEngineRepository.getEngine();
    }

    @Override
    public void ignitionOn() {
        EngineEntity engine = mEngineRepository.getEngine();
        if (engine == null || engine.state == EngineEntity.State.RUNNING) {
            return;
        }
        mEngineRepository.setEngineState(EngineEntity.State.RUNNING);
        mBatteryFeatureBoundary.addChargingRate(engine.chargeRate);
        mHeadlightFeatureBoundary.turnLightsDefaultState();
    }

    @Override
    public void ignitionOff() {
        EngineEntity engine = mEngineRepository.getEngine();
        if (engine == null || engine.state == EngineEntity.State.STOPPED) {
            return;
        }
        mEngineRepository.setEngineState(EngineEntity.State.STOPPED);
        mBatteryFeatureBoundary.addChargingRate(-engine.chargeRate);
        mHeadlightFeatureBoundary.turnLightsOff();
    }
}
