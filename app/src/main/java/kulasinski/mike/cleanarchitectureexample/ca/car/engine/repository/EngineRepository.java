package kulasinski.mike.cleanarchitectureexample.ca.car.engine.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;

public interface EngineRepository {

    void addEngine(@NonNull EngineEntity engine);

    @Nullable
    EngineEntity getEngine();

    void setEngineState(@NonNull EngineEntity.State state);

    class Factory {
        @NonNull
        public static EngineRepository create() {
            return new EngineRepositoryImpl();
        }
    }
}
