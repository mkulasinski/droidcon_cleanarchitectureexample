package kulasinski.mike.cleanarchitectureexample.ca.car.engine.presenter;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineFeatureBoundary;

class EnginePresenterImpl implements EnginePresenter {

    @NonNull
    private final View mView;
    @NonNull
    private final EngineFeatureBoundary mEngineFeatureBoundary;

    EnginePresenterImpl(@NonNull View view) {
        mView = view;
        mEngineFeatureBoundary = EngineFeatureBoundary.Factory.create();
    }

    @Override
    public void onRequestEngineInfo() {
        EngineEntity engine = mEngineFeatureBoundary.getEngine();
        if (engine != null) {
            mView.engineInfoAvailable(engine);
        }
    }

    @Override
    public void onTurnEngineOn() {
        mEngineFeatureBoundary.ignitionOn();
    }

    @Override
    public void onTurnEngineOff() {
        mEngineFeatureBoundary.ignitionOff();
    }
}
