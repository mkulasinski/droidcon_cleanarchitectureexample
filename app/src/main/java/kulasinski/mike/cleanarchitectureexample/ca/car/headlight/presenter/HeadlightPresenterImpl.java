package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.presenter;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightFeatureBoundary;

class HeadlightPresenterImpl implements HeadlightPresenter {

    @NonNull
    private final View mView;
    @NonNull
    private final HeadlightFeatureBoundary mHeadlightFeatureBoundary;

    HeadlightPresenterImpl(@NonNull View view) {
        mView = view;
        mHeadlightFeatureBoundary = HeadlightFeatureBoundary.Factory.create();
    }

    @Override
    public void onRequestHeadlightInfo() {
        HeadlightEntity headlights = mHeadlightFeatureBoundary.getHeadlights();
        if (headlights != null) {
            mView.headlightInfoAvailable(HeadlightPresenterModel.transform(headlights));
        }
    }

    @Override
    public void onTurnLightsOn(@NonNull HeadlightEntity.LightMode mode) {
        mHeadlightFeatureBoundary.turnLightsOn(mode, new HeadlightFeatureBoundary.NoPowerErrorCallback() {
            @Override
            public void insufficientPowerException() {
                mView.noPowerForHeadlights();
            }
        });
    }

    @Override
    public void onTurnLightsOff() {
        mHeadlightFeatureBoundary.turnLightsOff();
    }
}
