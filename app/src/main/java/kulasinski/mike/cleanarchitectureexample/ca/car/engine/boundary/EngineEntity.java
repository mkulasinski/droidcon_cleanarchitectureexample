package kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary;

import android.support.annotation.NonNull;

public class EngineEntity {

    @NonNull
    public final State state;
    public final int chargeRate;

    public EngineEntity(@NonNull State state, int chargeRate) {
        this.state = state;
        this.chargeRate = chargeRate;
    }

    public enum State {
        RUNNING,
        STOPPED
    }
}
