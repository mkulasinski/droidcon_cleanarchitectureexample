package kulasinski.mike.cleanarchitectureexample.ca.car.battery.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.boundary.BatteryEntity;

/**
 * composition of datasources, only memory is allowed for now. but in future you can add more and manage threading
 * here.
 */
class BatteryRepositoryImpl implements BatteryRepository {

    private final BatteryMemoryDataSource mMemoryDataSource;

    BatteryRepositoryImpl() {
        mMemoryDataSource = new BatteryMemoryDataSource();
        addBattery(new BatteryEntity(
                1000,
                1000,
                0,
                0
        ));
    }

    @Override
    public void addBattery(@NonNull BatteryEntity batteryEntity) {
        mMemoryDataSource.addBattery(batteryEntity);
    }

    @Nullable
    @Override
    public BatteryEntity getBattery() {
        return mMemoryDataSource.getBattery();
    }

    @Override
    public void updateBattery(@NonNull BatteryEntity entity, int chargeRateDelta, int dischargeRateDelta) {
        mMemoryDataSource.updateBattery(entity, chargeRateDelta, dischargeRateDelta);
    }
}
