package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface HeadlightFeatureBoundary {

    @Nullable
    HeadlightEntity getHeadlights();

    void turnLightsOn(@NonNull HeadlightEntity.LightMode mode, @NonNull NoPowerErrorCallback errorCallback);

    void turnLightsOff();

    void turnLightsDefaultState();

    interface NoPowerErrorCallback {
        void insufficientPowerException();
    }

    class Factory {
        public static HeadlightFeatureBoundary create() {
            return new HeadlightFeatureUseCase();
        }
    }
}