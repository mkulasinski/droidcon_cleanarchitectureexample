package kulasinski.mike.cleanarchitectureexample.ca.car.battery.ui.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import kulasinski.mike.cleanarchitectureexample.R;

class BatteryViewHolder {

    @NonNull
    final TextView mChargeLevelView;
    @NonNull
    final TextView mChargeDischargeRateView;

    public BatteryViewHolder(@NonNull View root) {
        View container = root.findViewById(R.id.batteryFeature);
        mChargeLevelView = (TextView) container.findViewById(R.id.batteryFeature_level);
        mChargeDischargeRateView = (TextView) container.findViewById(R.id.batteryFeature_deltaRate);
    }
}
