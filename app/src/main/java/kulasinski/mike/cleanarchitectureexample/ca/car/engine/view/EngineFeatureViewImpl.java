package kulasinski.mike.cleanarchitectureexample.ca.car.engine.view;

import android.support.annotation.NonNull;
import android.view.View;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;
import kulasinski.mike.cleanarchitectureexample.ca.car.engine.presenter.EnginePresenter;

public class EngineFeatureViewImpl {

    @NonNull
    private final EngineViewBinder mViewBinder;
    @NonNull
    private final EnginePresenter mPresenter;

    public EngineFeatureViewImpl(@NonNull View root) {
        mViewBinder = new EngineViewBinder(root, new EngineViewBinder.EngineSwitchChanged() {
            @Override
            public void onEngineSwitched(boolean isOn) {
                if (isOn) {
                    mPresenter.onTurnEngineOn();
                } else {
                    mPresenter.onTurnEngineOff();
                }
            }
        });
        mPresenter = EnginePresenter.Factory.create(getView());
    }

    public void requestInfo() {
        mPresenter.onRequestEngineInfo();
    }

    @NonNull
    private EnginePresenter.View getView() {
        return new EnginePresenter.View() {
            @Override
            public void engineInfoAvailable(@NonNull EngineEntity engineEntity) {
                mViewBinder.bind(engineEntity);
            }
        };
    }
}
