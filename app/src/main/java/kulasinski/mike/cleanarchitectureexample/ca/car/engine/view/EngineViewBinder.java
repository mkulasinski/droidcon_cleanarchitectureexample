package kulasinski.mike.cleanarchitectureexample.ca.car.engine.view;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CompoundButton;

import kulasinski.mike.cleanarchitectureexample.ca.car.engine.boundary.EngineEntity;

class EngineViewBinder {

    @NonNull
    private final EngineViewHolder mViewHolder;
    @NonNull
    private final EngineSwitchChanged mEngineSwitchChanged;

    public EngineViewBinder(@NonNull View view, @NonNull EngineSwitchChanged engineSwitchChanged) {
        mViewHolder = new EngineViewHolder(view);
        mEngineSwitchChanged = engineSwitchChanged;
    }

    public void bind(@NonNull EngineEntity engineEntity) {
        mViewHolder.mEngineSwitch.setOnCheckedChangeListener(null);
        mViewHolder.mEngineSwitch.setChecked(engineEntity.state == EngineEntity.State.RUNNING);
        mViewHolder.mEngineSwitch.setOnCheckedChangeListener(mOnCheckedChangeListener);
    }

    private final CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mEngineSwitchChanged.onEngineSwitched(isChecked);
        }
    };

    interface EngineSwitchChanged {
        void onEngineSwitched(boolean isOn);
    }
}
