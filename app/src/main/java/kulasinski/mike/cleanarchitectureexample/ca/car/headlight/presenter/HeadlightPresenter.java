package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.presenter;

import android.support.annotation.NonNull;

import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;

public interface HeadlightPresenter {

    void onRequestHeadlightInfo();

    void onTurnLightsOn(@NonNull HeadlightEntity.LightMode mode);

    void onTurnLightsOff();

    interface View {
        void headlightInfoAvailable(@NonNull HeadlightPresenterModel model);

        void noPowerForHeadlights();
    }

    class HeadlightPresenterModel {
        @NonNull
        public final HeadlightEntity.LightMode lightMode;
        @NonNull
        public final HeadlightEntity.State state;

        private HeadlightPresenterModel(@NonNull HeadlightEntity.LightMode lightMode,
                                        @NonNull HeadlightEntity.State state) {
            this.lightMode = lightMode;
            this.state = state;
        }

        public static HeadlightPresenterModel transform(@NonNull HeadlightEntity entity) {
            return new HeadlightPresenterModel(
                    entity.mode,
                    entity.state
            );
        }
    }

    class Factory {
        @NonNull
        public static HeadlightPresenter create(@NonNull View view) {
            return new HeadlightPresenterImpl(view);
        }
    }
}
