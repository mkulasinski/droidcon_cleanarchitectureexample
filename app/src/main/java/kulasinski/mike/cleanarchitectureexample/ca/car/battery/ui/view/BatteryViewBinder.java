package kulasinski.mike.cleanarchitectureexample.ca.car.battery.ui.view;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.presenter.BatteryFeaturePresenter;

class BatteryViewBinder {

    @NonNull
    private final BatteryViewHolder mViewHolder;

    public BatteryViewBinder(@NonNull View view) {
        mViewHolder = new BatteryViewHolder(view);
    }

    public void bind(@NonNull BatteryFeaturePresenter.BatteryPresenterModel model) {
        mViewHolder.mChargeLevelView.setText("" + ((int) model.percentage) + "%");
        mViewHolder.mChargeDischargeRateView.setText("" + model.chargeDelta);
    }

    public void changeColorToLow() {
        mViewHolder.mChargeDischargeRateView.setTextColor(Color.RED);
        mViewHolder.mChargeLevelView.setTextColor(Color.RED);
    }

    public void changeColorToNormal() {
        mViewHolder.mChargeDischargeRateView.setTextColor(Color.BLUE);
        mViewHolder.mChargeLevelView.setTextColor(Color.BLUE);
    }

    public void changeColorToHight() {
        mViewHolder.mChargeDischargeRateView.setTextColor(Color.GREEN);
        mViewHolder.mChargeLevelView.setTextColor(Color.GREEN);
    }
}
