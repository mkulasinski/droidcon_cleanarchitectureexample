package kulasinski.mike.cleanarchitectureexample.ca.car.headlight.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.boundary.HeadlightEntity;

/**
 * composition of datasources, only memory is allowed for now. but in future you can add more and manage threading
 * here.
 */
class HeadlightRepositoryImpl implements HeadlightRepository {

    private final HeadlightMemoryDataSource mMemoryDataSource;

    HeadlightRepositoryImpl() {
        mMemoryDataSource = new HeadlightMemoryDataSource();
        addHeadlight(new HeadlightEntity(
                HeadlightEntity.State.LIGHTS_OFF,
                HeadlightEntity.LightMode.LOW_BEAM,
                100,
                150,
                80));
    }

    @Override
    public void addHeadlight(@NonNull HeadlightEntity entity) {
        mMemoryDataSource.addHeadlight(entity);
    }

    @Nullable
    @Override
    public HeadlightEntity getHeadlight() {
        return mMemoryDataSource.getHeadlight();
    }

    @Override
    public void setHeadlightState(@NonNull HeadlightEntity.State state) {
        mMemoryDataSource.setHeadlightState(state);
    }

    @Override
    public void setHeadlightMode(@NonNull HeadlightEntity.LightMode mode) {
        mMemoryDataSource.setHeadlightMode(mode);
    }
}
