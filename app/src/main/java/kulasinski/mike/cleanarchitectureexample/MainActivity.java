package kulasinski.mike.cleanarchitectureexample;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import kulasinski.mike.cleanarchitectureexample.ca.car.battery.ui.view.BatteryFeatureViewImpl;
import kulasinski.mike.cleanarchitectureexample.ca.car.engine.view.EngineFeatureViewImpl;
import kulasinski.mike.cleanarchitectureexample.ca.car.headlight.ui.view.HeadlightFeatureViewImpl;

public class MainActivity extends AppCompatActivity {

    private BatteryFeatureViewImpl mBatteryFeatureView;
    private HeadlightFeatureViewImpl mHeadlightFeatureView;
    private EngineFeatureViewImpl mEngineFeatureView;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler = new Handler();

        View view = findViewById(android.R.id.content);
        mBatteryFeatureView = new BatteryFeatureViewImpl(view, new BatteryFeatureViewImpl.Callback() {
            @Override
            public void onBatteryEmpty() {
                mHeadlightFeatureView.requestInfo();
            }
        });
        mHeadlightFeatureView = new HeadlightFeatureViewImpl(view, new HeadlightFeatureViewImpl.Callback() {
            @Override
            public void noPowerForHeadlights() {
                Toast.makeText(MainActivity.this, "No power for lights", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onHeadlightStateChanged() {
                mBatteryFeatureView.requestInfo();
            }
        });
        mEngineFeatureView = new EngineFeatureViewImpl(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
        startRefreshRate();
    }

    private void refresh() {
        mBatteryFeatureView.requestInfo();
        mHeadlightFeatureView.requestInfo();
        mEngineFeatureView.requestInfo();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRefreshRate();
    }

    public void startRefreshRate() {
        mHandler.removeCallbacks(mTickRunnable);
        mHandler.postDelayed(mTickRunnable, 1000);
    }

    public void stopRefreshRate() {
        mHandler.removeCallbacks(mTickRunnable);
    }

    @NonNull
    private final Runnable mTickRunnable = new Runnable() {
        @Override
        public void run() {
            refresh();
            mHandler.postDelayed(this, 1000);
        }
    };
}
